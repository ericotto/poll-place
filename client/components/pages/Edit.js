import React from 'react';

class FormInput extends React.Component {
  render() {
    const option = this.props.option;
    return (
       <input type="text" id="options" className="form-control" placeholder={option} defaultValue={option} required></input>
    )
  }
}

class Edit extends React.Component {

  constructor(props) {
    super(props);
    const { pollId } = this.props.params;
    const i = this.props.poll.polls.findIndex((poll) => poll._id === pollId)
    this.state = {
      name: this.props.poll.polls[i].name,
      inputs: this.props.poll.polls[i].options.map(option => { return option.option }),
    }
  }

  handleEdit(event) {
    event.preventDefault();
    const { pollId } = this.props.params;
    const i = this.props.poll.polls.findIndex((poll) => poll._id === pollId);
    const name = event.target.name.value;
    let inputs = [];
    for (var iter in this.state.inputs) {
      inputs.push(event.target.options[iter].value)
    }
    let options = inputs.join(", ")
    this.props.editPoll(name, options, pollId, i, this.props.user.token);
  }

  addInput(event) {
    event.preventDefault();
    let newInput = 'Option ' + (this.state.inputs.length + 1);
    this.setState({
      inputs: this.state.inputs.concat([newInput])
    });
  }

  removeInput(event) {
    event.preventDefault();
    this.setState({
      inputs: this.state.inputs.slice(0, -1)
    });
  }


  render() {
    return (
      <div className="container">
        <div className="component-wrap col-lg-offset-4 col-lg-4 col-md-offset-4 col-md-4  col-sm-offset-3 col-sm-6 col-xs-offset-2 col-xs-8s">
        <h1 className="text-center">Edit Poll</h1>
        <form onSubmit={this.handleEdit.bind(this)}>
          { this.props.poll.createMessage ? <div className="alert-danger">{this.props.poll.createMessage}</div> : '' }
          <label>Question:</label>
          <input type="text" id="name" className="form-control" placeholder='Question' defaultValue={this.state.name} required />
          <label>Options:</label>
          { this.state.inputs.map( (input, i) => <FormInput key={i} option={input} value={input}/>)}
          <button className="btn btn-sm btn-block" onClick={this.addInput.bind(this)}>Add Option</button>
          <button className="btn btn-sm btn-block" onClick={this.removeInput.bind(this)}>Remove Option</button>
          <button className="btn btn-lg btn-primary btn-block" type="submit">Edit Poll</button>
        </form>
        </div>
      </div>
    )
  }
}

export default Edit;
