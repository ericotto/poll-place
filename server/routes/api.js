'use strict';

var express = require('express');
var api = express.Router();
var User = require('../models/userSchema');
var userRoute = require('./user');
var pollRoute = require('./poll');

api.get('/', function(req, res) {
  res.json({message: 'welcome to the API'});
});

api.use('/user', userRoute);
api.use('/poll', pollRoute);

module.exports = api;
