import React from 'react';

class Register extends React.Component {

  handleRegistration(event) {
    event.preventDefault();
    var username = event.target.username.value;
    var password = event.target.inputPassword.value;
    var confirmPassword = event.target.confirmPassword.value;
    this.props.register(username, password, confirmPassword);
    event.target.username.value = '';
    event.target.inputPassword.value = '';
    event.target.confirmPassword.value = '';
  }

  render() {
    return (
      <div className="container">
        <div className="component-wrap col-lg-offset-4 col-lg-4 col-md-offset-4 col-md-4  col-sm-offset-3 col-sm-6 col-xs-offset-2 col-xs-8">
        <h1 className="text-center">Sign Up</h1>
        <form onSubmit={this.handleRegistration.bind(this)}>
          { this.props.user.registerMessage ? <div className="alert-danger">{this.props.user.registerMessage}</div> : '' }
          <label htmlFor="username">Username:</label>
          <input type="text" id="username" className="form-control" placeholder="Username" required></input>
          <label htmlFor="inputPassword">Password:</label>
          <input type="password" id="inputPassword" className="form-control" placeholder="Password" required></input>
          <label htmlFor="confirmPassword">Confirm Password:</label>
          <input type="password" id="confirmPassword" className="form-control" placeholder="Password" required></input>
          <button className="btn btn-lg btn-primary btn-block" type="submit">Sign Up</button>
        </form>
        </div>
      </div>
    )
  }
}

export default Register;
