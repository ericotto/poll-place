import React from 'react';
import { ShareButtons, generateShareIcon } from 'react-share';

class Social extends React.Component  {
  render() {
    const {
      FacebookShareButton,
      TwitterShareButton,
      LinkedinShareButton
    } = ShareButtons;
    const FacebookIcon = generateShareIcon('facebook');
    const TwitterIcon = generateShareIcon('twitter');
    const LinkedinIcon = generateShareIcon('linkedin');
    return(
      <div className="row text-center social">
          <FacebookShareButton url={window.location.href}
            className="social-button"
            title="Vote on my Poll!">
            <FacebookIcon round={true} size={50}/>
          </FacebookShareButton>
          <TwitterShareButton url={window.location.href}
            className="social-button"
            title="Vote on my Poll!">
            <TwitterIcon round={true} size={50}/>
          </TwitterShareButton>
          <LinkedinShareButton url={window.location.href}
            className="social-button"
            title="Vote on my Poll!">
            <LinkedinIcon round={true} size={50}/>
          </LinkedinShareButton>
      </div>
    )
  }
}

export default Social;
