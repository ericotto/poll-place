import React from 'react';
import { Doughnut } from 'react-chartjs-2';
import Social from '../utils/Social';

class Poll extends React.Component {

  constructor() {
    super()
    this.state = {
      voted: false
    };
  }

  handleVote(event) {
    event.preventDefault();
    const { pollId } = this.props.params;
    const voteOption = event.target.option.value;
    const pollIndex = this.props.poll.polls.findIndex((poll) => poll._id === pollId);
    const optionIndex = this.props.poll.polls[pollIndex].options.findIndex((option) => option.option === voteOption)
    this.props.vote(pollId, voteOption, pollIndex, optionIndex);
    this.setState({voted: true});
  }

  getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  getPoll() {
    const { pollId } = this.props.params;
    const i = this.props.poll.polls.findIndex((poll) => poll._id === pollId);
    const poll = this.props.poll.polls[i];
    return poll;
  }

  showForm() {
    const poll = this.getPoll();
    return (
      <form id="vote_form" className={this.state.voted ? "form hidden" : "form"} onSubmit={this.handleVote.bind(this)}>
        <select className="form-control" name="option">
        { poll.options.map( function(option, i) {
            return(<option key={i} id={option.option} value={option.option}>{option.option}</option>)
          }
        )}
        </select>
        <button className="btn btn-md btn-block" type="submit">Vote</button>
      </form>
    )
  }

  showChart() {
    const poll = this.getPoll();
    const data = {
      labels: poll.options.map((option) => option.option),
      datasets: [{
        data: poll.options.map((option) => option.value),
        backgroundColor: poll.options.map(() => this.getRandomColor())
      }]
    };
    const options = {
      legend: {
        display: false
      },
      responsive: true,
      maintainAspectRatio: true
    }
    return (
        <Doughnut className="chart" data={data} options={options}/>
    );
  }

  render() {
    return (
      <div className="container">
      <div className="component-wrap col-lg-offset-4 col-lg-4 col-md-offset-4 col-md-4 col-sm-offset-3 col-sm-6 col-xs-offset-2 col-xs-8">
        <div className="text-center">
          { this.props.poll.polls ? <h1>{this.getPoll().name}</h1> : '' }
          { this.props.poll.polls || this.state.voted ? this.showForm() : <p>Loading...</p> }
          { this.state.voted ? <p>Thanks for Voting!</p> : '' }
        </div>
        <div className="graph">
          { this.props.poll.polls ? this.showChart() : ''}
        </div>
        <Social />
      </div>
    </div>
    )
  }
}

export default Poll;
