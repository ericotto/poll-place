import axios from 'axios';
import { browserHistory } from 'react-router';
import cookie from 'react-cookie';


export function login(username, password, remember_me) {
  return function(dispatch) {
    axios.post('/api/user/login',
      { username: username,
        password: password,
        remember_me: remember_me},
    ).then( response => {
      if (response.data.success) {
        dispatch({type: 'LOGIN_USER', token: response.data.token, username: response.data.username});
        browserHistory.push('/');
      }
      dispatch({type: 'LOGIN_MESSAGE', message: response.data.message});
    });
  }
}

export function tokenLogin(token) {
  return function(dispatch) {
    axios.get('/api/user/')
    .then( response => {
      if (response.data.success) {
        dispatch({type: 'LOGIN_USER', token: token, username: response.data.username});
      }
    });
  }
}

export function logout() {
  return function(dispatch) {
    dispatch({ type: 'LOGOUT_USER'});
    cookie.remove('jwt');
    browserHistory.push('/');
  }
}

export function register(username, password, confirmPassword) {
  return function(dispatch) {
    axios.post('/api/user/create',
      { username: username,
        password: password,
        confirmPassword: confirmPassword },
    ).then( response => {
      if (response.data.success) {
        dispatch({type: 'LOGIN_USER', token: response.data.token, username: response.data.username});
        browserHistory.push('/');
      }
      dispatch({type: 'REGISTER_MESSAGE', message: response.data.message});
    });
  }
}

export function getAllPolls() {
  return function(dispatch) {
    axios.get('/api/poll/', {})
      .then( response => {
        if(response.data.success) {
          dispatch({type: 'GET_ALL_POLLS', polls: response.data.polls});
        }
    });
  }
}

export function vote(pollId, option, pollIndex, optionIndex) {
  return function(dispatch) {
    axios.post('/api/poll/vote',
    { id: pollId,
      option: option})
    .then( response => {
      if (response.data.success) {
        dispatch({type: 'INCREMENT_POLL', i: pollIndex, option: optionIndex})
      }
    });
  }
}

export function createPoll(name, options, token) {
  return function(dispatch) {
    axios.post('/api/poll/create', {
      'name': name,
      'options': options,
      'jwt': token
    }).then( response => {
      if(response.data.success) {
        dispatch({type: 'CREATE_POLL', poll: response.data.poll})
        browserHistory.push('/poll/' + response.data.poll._id);
      } else {
        dispatch({type: 'POLL_CREATE_MESSAGE', message: response.data.messaage })
      }
    })
  }
}

export function deletePoll(pollId, i, token) {
  return function(dispatch) {
    axios.post('/api/poll/delete', {
      'jwt': token,
      'id': pollId
    }).then( response => {
      if (response.data.success) {
        dispatch({type: 'DELETE_POLL', i: i});
      }
    })
  }
}

export function editPoll(name, options, pollId, i, token) {
  return function(dispatch) {
    axios.post('/api/poll/edit' , {
      'name': name,
      'options': options,
      'id': pollId,
      'jwt': token
    }).then( response => {
      if (response.data.success) {
        console.log(response.data.poll)
        dispatch({type: 'EDIT_POLL', i: i, poll: response.data.poll})
        browserHistory.push('/poll/' + response.data.poll._id);
      }
    })
  }
}
