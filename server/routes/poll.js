var express = require('express');
var pollRoute = express.Router();
var Poll = require('../models/pollSchema');
var jwt = require('jsonwebtoken');
var passport = require('passport');

// get all polls
pollRoute.get('/', function(req, res) {
  Poll.find({}, function(err, polls) {
    if (err) {
      return res.json({success: false, message: "No polls"});
    }
    res.json({success: true, polls: polls});
  });
});

// vote for poll
pollRoute.post('/vote', function(req, res) {
  Poll.findOne({_id: req.body.id}, function(err, poll) {
    if (err) {
      return res.json({success: false, message: 'Failed to find poll'});
    }
    let updatedOptions = poll.options.map(function(option) {
      if (option.option === req.body.option) {
        option.value += 1
      }
      return option;
    });
    let updatePoll = {
      options: updatedOptions
    };
    Poll.update({_id: poll._id}, updatePoll, function(err) {
      if (err) {
        console.log(err);
        return res.json({success: false, message: 'Failed to vote on poll'});
      }
      res.json({success: true, message: 'Voted'});
    });
  })
});

// get user polls
pollRoute.get('/user', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  Poll.find({owner: req.user.username}, function(err, polls) {
    if (err) {
      return res.json({success: false, message: "No such user"});
    }
    res.json({success: true, polls: polls});
  });
});

// create poll (auth req)
pollRoute.post('/create', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  var options = getOptions(req.body.options);
  var newPoll = new Poll({
    name: req.body.name,
    owner: req.user.username,
    options: options
  });
  newPoll.save(function(err, poll) {
    console.log(poll);
    if (err) {
      return res.json({success: false, message: "Failed to create poll"});
    }
    res.json({success: true, poll: poll});
  });
});

// delete poll (auth req)
pollRoute.post('/delete', passport.authenticate('jwt', {
    session: false
  }), function(req, res) {
  console.log(req.body.id)
  Poll.findOne({
    _id: req.body.id
  }, function(err, poll) {
    if (err) {
      return res.json({success: false, message: 'Failed to delete poll'});
    }
    console.log(poll);
    var validateMessage = validatePoll(poll, req.user);
    console.log(validateMessage);
    if (validateMessage) return res.json(validateMessage);
    poll.remove();
    res.json({success: true, message: 'Poll deleted'});
  });
});

// edit poll (auth req)
pollRoute.post('/edit', passport.authenticate('jwt', {
  session: false
  }), function(req, res) {
  console.log(req);
  Poll.findOne({
    _id: req.body.id
  }, function(err, poll) {
    if (err) {
      return res.json({success: false, message: 'Failed to edit poll'});
    }
    var validateMessage = validatePoll(poll, req.user);
    if (validateMessage) return res.json(validateMessage);
    let updateOptions = getOptions(req.body.options);
    for (option1 in poll.options) {
    	for (option2 in updateOptions) {
      	if (poll.options[option1].option === updateOptions[option2].option) {
    			updateOptions[option2] = poll.options[option1];
        }
      }
    }
    var updatePoll = {
      name: req.body.name,
      options: updateOptions
    };
    Poll.findOneAndUpdate({_id: poll._id}, updatePoll, { new: true }, function(err, result) {
      if (err) {
        return res.json({success: false, message: 'Failed to edit poll'});
      }
      res.json({success: true, poll: result});
    });
  });
});


// helpers
function validatePoll(poll, user) {
  if (poll === null) {
    return {success: false, message: 'Poll does not exist'};
  }
  if (user.username != poll.owner) {
    return {success: false, message: 'Not authorized'};
  }
  return '';
}

function getOptions(reqOptions) {
  var options = []
  var reqOptions = reqOptions.split(', ');
  for (var i in reqOptions) {
    options.push({option: reqOptions[i]});
  }
  return options;
}

module.exports = pollRoute;
