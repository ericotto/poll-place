import React from 'react';
import PollCard from '../utils/PollCard';

class Main extends React.Component {
  render() {
    return (
      <div className="text-center component-wrap col-lg-offset-3 col-lg-6 col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8 col-xs-offset-2 col-xs-8">
        <h1>Polls</h1>
        { this.props.poll.polls ? this.props.poll.polls.map((poll, i) =>
            <PollCard {...this.props} key={i} i={i}/>)
          : <p>Loading...</p> }
      </div>
    )
  }
}


export default Main;
