import React from 'react';
import UserPollCard from '../utils/UserPollCard'
import { Link } from 'react-router';

class Dashboard extends React.Component {
  render() {
    let username = this.props.user.username;
    return (
      <div className="component-wrap col-lg-offset-3 col-lg-6 col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8 col-xs-offset-2 col-xs-8">
        <h1 className="text-center">My Polls</h1>
        { this.props.poll.polls ? this.props.poll.polls.map( (poll, i) => {
            if (poll.owner === username) {
              return <UserPollCard {...this.props} key={i} i={i} />;
            }
          })
          : <p>Loading...</p> }
        <div>
          <Link to="/poll/create">
            <button className="btn btn-lg btn-primary center-block">Create New Poll</button>
          </Link>
        </div>
      </div>
    )
  }
}

export default Dashboard;
