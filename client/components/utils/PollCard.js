import React from 'react';
import { Link } from 'react-router';

class PollCard extends React.Component {
  render() {
    var i = this.props.i;
    return (
      <div className="pollCard row">
        <div className="well well-sm clearfix">
          <Link to={`/poll/${this.props.poll.polls[i]._id}`}>
              <p>{this.props.poll.polls[i].name}</p>
          </Link>
          <Link to={`/poll/${this.props.poll.polls[i]._id}`}>
            <button className="btn btn-sm">Vote</button>
          </Link>
        </div>
      </div>
    )
  }
}

export default PollCard;
