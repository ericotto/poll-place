import React from 'react';

class Logout extends React.Component {
  componentWillMount() {
    this.props.logout();
  }

  render() {
    return (
      <div>Logging Out</div>
    )
  }
}


export default Logout;
