function poll(state = [], action) {
  switch(action.type) {
    case 'GET_ALL_POLLS':
      return {...state,
        polls: action.polls
      };
    case 'INCREMENT_POLL':
      return {...state,
        ...state.polls[action.i].options[action.option].value += 1
      };
    case 'CREATE_POLL':
      return {...state,
        ...state.polls.push(action.poll)
      };
    case 'POLL_CREATE_MESSAGE':
     return {...state,
       createMessage: action.message
     };
    case 'DELETE_POLL':
      return {...state,
        ...state.polls.splice(action.i, 1)
      };
    case 'EDIT_POLL':
      return {...state,
        ...state.polls[action.i] = action.poll
      };
    default:
      return state;
  }
}

export default poll;
