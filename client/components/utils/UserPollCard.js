import React from 'react';
import { Link } from 'react-router';

class UserPollCard extends React.Component {

  deletePoll(event) {
    event.preventDefault();
    const i = this.props.i;
    const pollId = this.props.poll.polls[i]._id;
    const token = this.props.user.token;
    this.props.deletePoll(pollId, i, token);
  }

  render() {
    const i = this.props.i;
    return (
      <div className="pollCard row">
        <div className="well well-sm clearfix">
          <Link to={`/poll/${this.props.poll.polls[i]._id}`}>
              <p>{this.props.poll.polls[i].name}</p>
          </Link>
          <Link to={`/poll/${this.props.poll.polls[i]._id}/edit`}>
          <button className="btn btn-sm">Edit</button>
          </Link>
          <button className="btn btn-sm btn-danger" onClick={this.deletePoll.bind(this)}>Delete</button>
        </div>
      </div>
    )
  }
}

export default UserPollCard;
