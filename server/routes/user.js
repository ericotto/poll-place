'use strict';

var express = require('express');
var userRoute = express.Router();
var User = require('../models/userSchema');
var jwt = require('jsonwebtoken');
var passport = require('passport');
var cookieParser = require('cookie-parser');
var SECRET = process.env.SECRET || require('../../config').SECRET;
var JWT_LIMIT = process.env.JWT_LIMIT || require('../../config').JWT_LIMIT;
var COOKIE_LIMIT = process.env.COOKIE_LIMIT || require('../../config').COOKIE_LIMIT;

var createToken = function(userObj) {
  var userSignObj = {
    username: userObj.username,
  }
  var token = jwt.sign(userSignObj, SECRET, {expiresIn: JWT_LIMIT});
  return token;
}

userRoute.get('/', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  res.status(200).send({success: true, username: req.user.username});
});

userRoute.post('/login', function(req, res) {
  User.findOne({
    username: req.body.username
  }, function(err, user) {
    if (err) throw err;
    if (!user) {
      res.json({success: false, message: "Login failed"});
    } else {
      user.comparePassword(req.body.password, function(err, isMatch) {
        if (isMatch && !err) {
          var token = createToken(user);
          if (req.body.remember_me) {
            res.cookie('jwt', token, { maxAge: COOKIE_LIMIT });
          }
          res.status(200).send({success: true, token: token, username: user.username});
        } else {
          res.json({success: false, message: "Login failed"});
        }
      })
    }
  });
});

// userRoute.post('/logout', passport.authenticate('jwt', {
//   session: false
// }), function(req, res) {
//   res.clearCookie('jwt');
//   res.status(200).send({success: true});
// });

userRoute.post('/create', function(req, res) {
  if (!req.body.username || !req.body.password) {
    res.json({success: false, message: "Please enter username and password."});
  } else  if (req.body.password !== req.body.confirmPassword) {
    res.json({success: false, message: "Passwords do not match."});
  } else {
    var newUser = new User({
      username: req.body.username,
      password: req.body.password
    });
    newUser.save(function(err) {
      if (err) {
        return res.json({success: false, message: "Username already exists."});
      };
      var token = createToken(newUser);
      //removed cookies on first login
      //res.cookie('jwt', token, config.cookieOpts);
      res.status(200).send({success: true, token: token, username: newUser.username});
    });
  }
});

module.exports = userRoute;
