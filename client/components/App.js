import React from 'react'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/actionCreators';
import Header from './utils/Header';

class Home extends React.Component {

  componentWillMount() {
    this.props.getAllPolls();
    if (this.props.user.token) {
      this.props.tokenLogin(this.props.user.token);
    }
  }

  render() {
    return (
      <div>
        <Header loggedIn={this.props.user.loggedIn}/>
        <div className="container">
        {React.cloneElement({...this.props}.children, {...this.props})}
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
    poll: state.poll
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actionCreators, dispatch);
}

const App = connect(mapStateToProps, mapDispatchToProps)(Home);

export default App;
