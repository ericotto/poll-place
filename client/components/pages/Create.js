import React from 'react';

class FormInput extends React.Component {
  render() {
    const option = this.props.option;
    //const id = 'option' + this.props.i;
    return (
      <input type="text" id="options" className="form-control" placeholder={option} required></input>
    )
  }
}

class Create extends React.Component {

  constructor() {
    super();
    this.state = {
      inputs: ['Option 1', 'Option 2']
    };
  }

  handleCreate(event) {
    event.preventDefault();
    const name = event.target.name.value;
    let options = [];
    for (var i in this.state.inputs) {
      options.push(event.target.options[i].value)
    }
    options = options.join(", ");
    this.props.createPoll(name, options, this.props.user.token);
  }

  addInput(event) {
    event.preventDefault();
    let newInput = 'Option ' + (this.state.inputs.length + 1);
    this.setState({
      inputs: this.state.inputs.concat([newInput])
    });
  }

  removeInput(event) {
    event.preventDefault();
    if (this.state.inputs.length > 2) {
      this.setState({
        inputs: this.state.inputs.slice(0, -1)
      });
    }
  }


  render() {
    return (
      <div className="container">
        <div className="component-wrap col-lg-offset-4 col-lg-4 col-md-offset-4 col-md-4  col-sm-offset-3 col-sm-6 col-xs-offset-2 col-xs-8s">
        <h1 className="text-center">Create Poll</h1>
        <form onSubmit={this.handleCreate.bind(this)}>
          { this.props.poll.createMessage ? <div className="alert-danger">{this.props.poll.createMessage}</div> : '' }
          <label>Question:</label>
          <input type="text" id="name" className="form-control" placeholder="Question to answer" required></input>
          <label>Options:</label>
          { this.state.inputs.map( (input, i) => <FormInput key={i} option={input}/> ) }
          <button className="btn btn-sm btn-block" onClick={this.addInput.bind(this)}>Add Option</button>
          <button className="btn btn-sm btn-block" onClick={this.removeInput.bind(this)}>Remove Option</button>
          <button className="btn btn-lg btn-primary btn-block" type="submit">Create Poll</button>
        </form>
        </div>
      </div>
    )
  }
}

export default Create;
