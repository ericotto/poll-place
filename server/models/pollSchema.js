var mongoose = require('mongoose');
var shortid = require('shortid');

var PollSchema = new mongoose.Schema({
  _id: {
    type: String,
    default: shortid.generate
  },
  name: {
    type: String,
    required: true,
  },
  owner: {
    type: String,
    required: true
  },
  options: [{
    option: String,
    value: {
      type: Number,
      default: 0
    }
  }]
});

module.exports = mongoose.model('Poll', PollSchema);
